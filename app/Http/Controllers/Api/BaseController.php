<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class BaseController extends Controller
{

  protected $request;

  public function __construct(Request $request)
  {
    $this->request = $request;
  }

  public function successResponse($data, int $status = 200)
  {
    return response()->json([
      'data' => $data
    ], $status);
  }

  public function errorResponse($message = 'server error', int $status = 400)
  {
    return response()->json(
      [
        'data' => $message
      ], $status
    );
  }
}