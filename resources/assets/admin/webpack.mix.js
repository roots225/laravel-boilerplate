const mix = require('laravel-mix');
const adminPublicPath = '../../../public/assets/admin/';

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.sass('sass/main.scss', adminPublicPath + 'css/admin.css');

mix.js('js/main.js', adminPublicPath + 'js/admin.js');