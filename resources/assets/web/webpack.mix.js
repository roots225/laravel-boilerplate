const mix = require('laravel-mix');
const webPublicPath = '../../../public/assets/web/';

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('js/app.js', webPublicPath + 'js/app.js')
   .sass('sass/app.scss', webPublicPath + 'css/app.css');