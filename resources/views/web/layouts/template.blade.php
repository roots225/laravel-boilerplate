<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'African Manager Conference') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <!-- <link rel="stylesheet" type="text/css" href="styles/bootstrap4/bootstrap.min.css">
		<link href="plugins/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css"> -->
		<!-- <link rel="stylesheet" type="text/css" href="plugins/OwlCarousel2-2.2.1/owl.carousel.css">
		<link rel="stylesheet" type="text/css" href="plugins/OwlCarousel2-2.2.1/owl.theme.default.css">
		<link rel="stylesheet" type="text/css" href="plugins/OwlCarousel2-2.2.1/animate.css">
		<link href="plugins/colorbox/colorbox.css" rel="stylesheet" type="text/css">
		<link rel="stylesheet" type="text/css" href="styles/main_styles.css">
		<link rel="stylesheet" type="text/css" href="styles/responsive.css">
		<link rel="stylesheet" type="text/css" href="styles/abouts.css">
		<link rel="stylesheet" type="text/css" href="styles/about_responsive.css"> -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
  <div id="app">
    <div class="super_container">
      @include('parts._header')

      <div class="menu d-flex flex-column align-items-center justify-content-center">
        <div class="menu_content">
            <div class="cross_1 d-flex flex-column align-items-center justify-content-center">
              <img src="http://amc.bestcomevents.com/images/'' $logo_mb->logo; ?>" alt="'' $logo_mb->name; ?>" title="'' $logo_mb->name; ?>">
              <div class="logo_text" style="display:inline;font-size: 19px;">
                '' $logo_mb->texte; ?> '' $logo_mb->name; ?><br>
                <span style="line-height:0;margin-right: 4px;text-align: center;">'' $logo_mb->texte; ?></span>
              </div>
            </div>
          
          <nav class="menu_nav">
            <ul>
              <li class="active"><a data-scroll href="#'' $nav_mobile->lien_nav; ?>">'' $nav_mobile->menu_nav; ?></a></li>

              <button type="button" class="btn btn-danger btn-lg" data-toggle="modal" data-target="#exampleModal" data-whatever="@mdo">Inscription</button>
            </ul>
          </nav>
        </div>
        <div class="menu_close"><i class="fa fa-times" aria-hidden="true"></i></div>
      </div>
      
      @yield('content')

      @include('parts._footer')
    </div>
    
    <!---modal inscription -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content" style="width:600px;">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel" style="text-align: center; font-weight: bold; font-size: 30px; color:black;">
            Inscription</h5>

            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <img src="http://amc.bestcomevents.com/images/africanmanager.jpg" title="BestCom Events" alt="BestCom Events" width="585px;"height="200px" style="margin: 0 auto; display: block;">
          <div class="modal-body">
            <form action="inscription.php" method="POST">
              <div class="form-row">

                <div class="form-group col-md-6">
                  <input type="text" name="nom" class="form-control" placeholder="Nom"  required>
                </div>

                <div class="form-group col-md-6">
                  <input type="text" name="prenom" class="form-control" placeholder="Prénom" required>
                </div>

                <div class="form-group col-md-6">
                  <input type="tel" name="telephone" class="form-control" placeholder="téléphone" required>
                </div>

                <div class="form-group col-md-6">
                  <input type="email" name="email" class="form-control" placeholder="E-Mail">
                </div>

                <div class="form-group col-md-6">
                  <input type="text" name="entreprise" class="form-control" placeholder="Entreprise/Organisation" required>
                </div>

                <div class="form-group col-md-6">
                  <input type="text" name="fonction" class="form-control" placeholder="Votre Fonction" required>
                </div>

              </div>

              <div class="modal-footer">

                <button type="submit" name="inscris" class="btn btn-success" style="width:300px;margin: 0 auto;">S'inscrire</button>
              </div>
            </form>

          </div>



        </div>
      </div>
    </div>
    <!---modal inscription -->
  </div>
  <script src="{{ asset('js/jquery-3.2.1.min.js') }}"></script>
  <script src="{{ asset('styles/bootstrap4/popper.js') }}"></script>
  <script src="{{ asset('styles/bootstrap4/bootstrap.min.js') }}"></script>
  <script src="{{ asset('plugins/OwlCarousel2-2.2.1/owl.carousel.js') }}"></script>
  <script src="{{ asset('plugins/easing/easing.js') }}"></script>
  <script src="{{ asset('plugins/parallax-js-master/parallax.min.js') }}"></script>
  <script src="{{ asset('plugins/colorbox/jquery.colorbox-min.js') }}"></script>
  <script src="{{ asset('js/custom.js') }}"></script>
  <script src="{{ asset('js/sermons.js') }}"></script>
</body>
</html>
