@extends('layouts.template')

@section('content')
<div class="home">
  <!-- Home Background-->
   while ($home_image = $home_background->fetch(PDO::FETCH_OBJ)){ ?>

    <div class="home_background" style="background-image:url(http://amc.bestcomevents.com/images/'' $home_image->image_bg; ?>)">

    </div>
   } ?>

  <div class="home_content text-center">
    <div class="container">
      <div class="row">
        <div class="col">
           while ($home_bgcont = $home_background_content->fetch(PDO::FETCH_OBJ)){ ?>

            <div class="home_title">'' $home_bgcont->home_title; ?></div>
            <div class="home_text">'' $home_bgcont->home_text; ?></div>
            <button type="button" class="btn btn-danger btn-lg" data-toggle="modal" data-target="#exampleModal" data-whatever="@mdo">
              Inscription
            </button>


          </div>
         } ?>
      </div>
    </div>
  </div>
</div>

<!-- Event -->
<div class="event">
  <div class="container">
    <div class="row">
      <div class="col">
        <div class="event_container d-flex flex-lg-row flex-column align-items-center justify-content-start">
          <div class="event_date d-flex flex-column align-items-center justify-content-center">
             while ($evda = $date_event->fetch(PDO::FETCH_OBJ)){ ?>
              <div class="'' $evda->class_fa; ?>">'' $evda->name; ?></div>
             } ?>
          </div>
          <div class="event_content">
             while ($titre_event = $title_event->fetch(PDO::FETCH_OBJ)){ ?>

              <div class="event_title">'' $titre_event->event_grand_title; ?> <a href="#">'' $titre_event->titre_event; ?></a>

              </div>
             } ?>


            <ul class="event_row">
               while ($heulieu = $event_heures->fetch(PDO::FETCH_OBJ)){ ?>
                <li>
                  <div class="event_icon"><img src="images/calendar.png" alt=""></div>
                  <span style="font-size:20px;">'' $heulieu->heure_debu_fin; ?></span>
                </li>
                <li>
                  <div class="event_icon"><img src="images/location.png" alt=""></div>
                  <span style="font-size:20px;">'' $heulieu->lieux; ?></span>
                </li>
               } ?>
            </ul>
          </div>
          <div class="event_timer_container ml-lg-auto">
            <ul class="event_timer">
              <li><div id="day" class="event_num">00</div><div class="event_ss">jours</div></li>
              <li><div id="hour" class="event_num">00</div><div class="event_ss">heures</div></li>
              <li><div id="minute" class="event_num">00</div><div class="event_ss">min</div></li>
              <li><div id="second" class="event_num">00</div><div class="event_ss">sec</div></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- About -->
<div class="about" id="apropos">
  <div class="container">
    <div class="row">
      <div class="col">
        <div class="section_title_container text-center">
           while ($title_about = $about->fetch(PDO::FETCH_OBJ)){ ?>
            <div class="section_title">'' $title_about->titre; ?></div>
           } ?>

        </div>
      </div>
    </div>
    <div class="row about_row">
      <div class="col-lg-6">
         while ($contenu = $cont_about->fetch(PDO::FETCH_OBJ)){ ?>
          <div class="about_image"><img src="http://amc.bestcomevents.com/images/'' $contenu->image; ?>" alt="BestCom Events" title="BestCom Events"></div>
        </div>
        <div class="col-lg-6">
          <div class="about_content">
            <div class="about_text">
              <p>'' $contenu->description; ?></p>
            </div>

            <div class="button about_button" style="width: 230px;margin:0 auto;"><a href="http://amc.bestcomevents.com/upload_doc/'' $contenu->file_doc; ?>">Télécharger la brochure</a></div>
          </div>
        </div>
       } ?>
    </div>
  </div>
</div>

<!-- Sermons -->


<div class="sermons">
  <div class="container">
    <div class="row">
      <div class="col">
        <div class="section_title_container text-center">
           while ($title_video = $titre_video->fetch(PDO::FETCH_OBJ)){ ?>
            <div class="section_title">'' $title_video->titre_video; ?></div>
           } ?>
          <div class="section_subtitle"></div>
        </div>
      </div>
    </div>
    <br><br>
    <div class="row latest_row">
      <div class="col-lg-10 offset-lg-1">
        <div class="latest_play d-flex flex-column align-items-center justify-content-center">
           while ($video = $home_video->fetch(PDO::FETCH_OBJ)){ ?>
            '' $video->iframe; ?>
           } ?>
        </div>
      </div>
    </div>


  </div>
</div>

<!-- Mission -->

<div class="mission" id="programme">
   while ($bg_program = $programe_bg->fetch(PDO::FETCH_OBJ)){ ?>

    <h1 class="text-center" style="color:white;font-size:42px;text-align:center;margin-top:-65px;">'' $bg_program->title; ?></h1>

    <div class="mission_background parallax-window" data-parallax="scroll" data-image-src="http://amc.bestcomevents.com/images/'' $bg_program->image; ?>" data-speed="0.8">

    </div>
   } ?>


  <div class="mission_items d-flex flex-row flex-wrap align-items-start justify-content-start">
     while ($leprogm = $programme->fetch(PDO::FETCH_OBJ)){ ?>
      <div class="mission_item text-center">

        <div class="mission_icon"><i class="'' $leprogm->mission_icon; ?>" aria-hidden="true"></i></div>
        <div class="mission_title">'' $leprogm->mission_title; ?></div>
        <div class="mission_text">'' $leprogm->mission_text; ?></div>
      </div>
     } ?>

    <br>

  </div>
  <div class="" style="margin:0 auto;text-align:center;color:white;margin-top:40px;">
    <button type="button" class="btn btn-danger" style="height:48px;width:210px;">
      <a href="http://amc.bestcomevents.com/upload_doc/TDR-AMC-1-.pdf">Programme</a></button>
    </div>
  </div>

  <!-- Team -->

  <div class="team">
    <div class="container">
      <div class="row">
        <div class="col">
          <div class="section_title_container text-center">
             while ($equipe = $equipe_titre->fetch(PDO::FETCH_OBJ)){ ?>
              <div class="section_title">'' $equipe->titre; ?></div>
              <div class="section_subtitle">'' $equipe->sous_titre; ?></div>
             } ?>
          </div>
        </div>
      </div>
      <div class="row team_row">

        <!-- Team Item -->
         while ($equipes = $conferencier->fetch(PDO::FETCH_OBJ)){ ?>
          <div class="col-lg-4 team_col">
            <div class="team_item">
              <div class="team_image">

                <img src="http://amc.bestcomevents.com/images/'' $equipes->team_image; ?>" alt="" title="">
                <div class="team_overlay d-flex flex-column align-items-center justify-content-end text-center trans_200">
                  <div class="team_overlay_text">
                    <p>'' $equipes->biographie; ?></p>
                  </div>
<!--<div class="team_overlay_contact">
<ul>
<li>
<i class="fa fa-facebook" aria-hidden="true"></i>
<div> : <a href="#" target="_blank">'' $equipes->facebook; ?></a></div>
</li>
<li>
<i class="fa fa-linkedin" aria-hidden="true"></i>
<div>: <a href="#" target="_blank">'' $equipes->linkedin; ?></a></div>
</li>
</ul>
</div>-->
</div>
</div>
<div class="team_content text-center">
<div class="team_name">'' $equipes->team_name; ?></div>
<div class="team_title">'' $equipes->team_fonction; ?></div>
</div>
</div>
</div>
 } ?>

</div>

</div>
</div>

<div class="about" id="Medias">
<div class="container">
<div class="row">
<div class="col">
<div class="section_title_container text-center">
 while ($medti = $media_tilte->fetch(PDO::FETCH_OBJ)){ ?>
<div class="section_title">'' $medti->titre; ?></div>
 } ?>

</div>
</div>
</div>
<div class="row about_row">
<div class="col-lg-6">
 while ($contmedia = $media->fetch(PDO::FETCH_OBJ)){ ?>
<div class="about_image"><img src="http://amc.bestcomevents.com/images/'' $contmedia->image; ?>" alt="BestCom Events" title="BestCom Events"></div>
</div>
<div class="col-lg-6">
<div class="about_content">
<div class="about_text">
<p>'' $contmedia->texte; ?></p>
</div>

<div class="button about_button" style="width: 230px;margin:0 auto;"><a href="http://amc.bestcomevents.com/upload_doc/'' $contmedia->upload_fi; ?>">Télécharger</a></div>
</div>
</div>
 } ?>
</div>
</div>
</div>

<div class="container" id="partenaires">
<div class="row">
<div class="col-md-12">
<h2 class="section-title wow" style="color: #2d2d2d;
font-family: open sans,sans-serif;
text-align: center;
font-weight: 700;
font-size: 50px;
padding: 15px 0;
margin-bottom: 0;
margin-top: 0;"> Nos Partenaires</h2>
</div>


 while ($parnaire = $partner->fetch(PDO::FETCH_OBJ)){ ?>
<div class="col-md-3 col-sm-6 col-xs-12">

<div class="spnsors-logo wow fadeInUp animated" data-wow-delay="0.1s" style="visibility: visible;-webkit-animation-delay: 0.1s; -moz-animation-delay: 0.1s; animation-delay: 0.1s;">
<a href="#"><img src="http://amc.bestcomevents.com/images/sponsors/'' $parnaire->image; ?>" alt="'' $parnaire->title; ?>" title="'' $parnaire->title; ?>"></a>
</div>

</div>
 } ?>

</div>
</div>



<!-- Quote -->

<!-- Events -->

<br>
<!-- Donations -->


<div class="container" style="margin:0 auto;">
<h3 class="text-center;"style="color: #2d2d2d;
font-family: open sans,sans-serif;
text-align: center;
font-weight: 700;
font-size: 50px;
padding: 15px 0;
margin-bottom: 0;
margin-top: 0;">Gallerie Events Day</h3>
<div class="row" style="margin:0 auto;">

<div id="carouselExampleFade" class="carousel slide carousel-fade" data-ride="carousel" style="margin:0 auto;">
<div class="carousel-inner" style="margin:0 auto;width:837px;height:472px;">

 while ($galery = $gallerie->fetch(PDO::FETCH_OBJ)){ ?>

<div class="carousel-item active">
<img class="d-block w-100" src="http://amc.bestcomevents.com/images/'' $galery->images; ?>" alt="'' $galery->title_img; ?>" title="'' $galery->title_img; ?>">
</div>
 } ?>

 while ($galy = $gallery->fetch(PDO::FETCH_OBJ)){ ?>

<div class="carousel-item">
<img class="d-block w-100" src="http://amc.bestcomevents.com/images/'' $galy->images; ?>" alt="'' $galy->title_img; ?>" title="'' $galy->title_img; ?>">
</div>
 } ?>

</div>
<a class="carousel-control-prev" href="#carouselExampleFade" role="button" data-slide="prev">
<span class="carousel-control-prev-icon" aria-hidden="true"></span>
<span class="sr-only">Previous</span>
</a>
<a class="carousel-control-next" href="#carouselExampleFade" role="button" data-slide="next">
<span class="carousel-control-next-icon" aria-hidden="true"></span>
<span class="sr-only">Next</span>
</a>
</div>
</div>
</div>
@endsection
