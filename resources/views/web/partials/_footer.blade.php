<footer class="footer">
  <div class="footer_background parallax-window" data-parallax="scroll" data-image-src="images/footer.jpg" data-speed="0.8"></div>
    <div class="container">
      <div class="row">
        <div class="col-lg-4 footer_col">
          <div class="footer_column footer_contact_column">
            <div class="footer_logo_container">
              <a href="#">
                <div class="footer_logo_text" style="display:inline;font-size: 22px;" >African Manager's Conference</div>
              </a>
            </div>

            <div class="footer_contact">
              <ul>
                <li>
                  <div>
                    <i class="fa fa-map-marker" aria-hidden="true"></i>
                  </div>
                  <span style="font-size:19px;">Abidjan 2 Plateaux Latrille, face CELPAID</span>
                </li>
                <li>
                  <div>
                    <i class="fa fa-phone" aria-hidden="true"></i>
                  </div>
                  <span style="font-size:19px;">+225 08 28 92 02 / 40 19 81 92</span>
                </li>
                <li>
                  <div>
                    <i class="fa fa-envelope" aria-hidden="true"></i>
                  </div>
                  <span style="font-size:19px;">amc@bestcomevents.com</span>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>

    
    <div class="row" id="contact">
      <form method="POST" action="">
        <h3 style="color:white;font-size:20px;text-align:center;">Contactez-Nous</h3><br>
        <div class="form-row">
          <div class="form-group col-md-6">
            <input type="text" class="form-control" id="inputEmail4" placeholder="Nom"  name="nom" required=>
          </div>

          <div class="form-group col-md-6">
            <input type="email" class="form-control" id="inputPassword4" placeholder="E-Mail" name="email" required="">
          </div>
        </div>

        <div class="form-group">
          <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" name="message" placeholder="Votre Message ..." required></textarea>
        </div>

        <button type="submit" class="btn btn-primary btn-lg">Envoyer</button>
      </form>
    </div>
    
  
    <div class="row copyright_row">
      <div class="col">
        <div class="text-center">	
          <h3 class="footer_logo_text" style="display:inline;font-size: 17px;"><span style="text-transform:none;">	Organisé par</span> Best Communication Events & LVPS Voyage</h3>
        </div>

        <div class="copyright_container d-flex flex-lg-row flex-column align-items-center justify-content-lg-start justify-content-center">
          <div class="copyright">
            AFRICAN MANAGER CONFERENCE
          </div>
          <div class="footer_social ml-lg-auto">
            <ul>
              <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
              <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
              <li><a href="#"><i class="fa fa-pinterest" aria-hidden="true"></i></a></li>
              <li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
              <li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
</footer>