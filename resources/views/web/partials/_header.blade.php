<header class="header">
  <div class="top_bar">
    <div class="top_bar_background" style="background-image:url(images/top_bar.jpg)"></div>
    <div class="top_bar_container">
      <div class="container">
        <div class="row">
          <div class="col">
            <div class="top_bar_content d-flex flex-row align-items-center justify-content-start">
              <ul class="top_bar_contact_list">
                <li>
                  <i class="" aria-hidden="true"></i>
                  <div> '' $infotop->info; ?> '' $infotop->nom; ?></div>
                </li>
                
                <li>
                  <i class="'' $infotop->class_fa; ?>" aria-hidden="true"></i>
                  <div> '' $infotop->info; ?> '' $infotop->nom; ?></div>
                </li>
              </ul>

              <div class="top_bar_social ml-auto">
                <ul class="social_list">
                  <li><a href="'' $social->lien_social; ?>" target="_blank"><i class="'' $social->class_fa; ?>" aria-hidden="true"></i></a></li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>				
  </div>

  <div class="header_container">
    <div class="container">
      <div class="row">
        <div class="col">
          <div class="header_content d-flex flex-row align-items-center justify-content-start">
            <div class="logo_container">
              while ($logo = $logo_pages->fetch(PDO::FETCH_OBJ)){ ?>
                <a href="http://amc.bestcomevents.com">
                  <div class="logo"><img src="http://amc.bestcomevents.com/images/'' $logo->logo; ?>" alt="'' $logo->name; ?>" title="'' $logo->name; ?>">  </span></div>
                  <div class="logo_text" style="font-size: 18px;">'' $logo->name; ?><br>'' $logo->texte; ?></div>
                </a>
              } ?>
              <br>
            </div>
            <nav class="main_nav_contaner ml-auto">
              <ul class="main_nav">
                while ($nav = $main_menu->fetch(PDO::FETCH_OBJ)){ ?>
                  <li class=""><a data-scroll href="#'' $nav->lien_nav; ?>">'' $nav->menu_nav; ?></a></li>
                } ?>

                <button type="button" class="btn btn-danger btn-lg" data-toggle="modal" data-target="#exampleModal" data-whatever="@mdo">Inscription</button>

                <!--<button type="button" class="btn btn-danger btn-lg">Inscription</button>-->
              </ul>

            </nav>

            <!-- Hamburger -->

            <div class="hamburger ml-auto">
              <i class="fa fa-bars" aria-hidden="true"></i>
            </div>

          </div>

        </div>
      </div>
    </div>
  </div>
</header>