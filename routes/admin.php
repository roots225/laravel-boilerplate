<?php

Route::resources([
  'configuration' => 'ConfigurationController',
  'speaker' => 'SpeakerController',
  'partner' => 'PartnerController',
  'gallery' => 'GalleryController',
]);